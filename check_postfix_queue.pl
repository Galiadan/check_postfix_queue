#!/usr/bin/perl -w

use strict;
use warnings FATAL => 'all';
use Getopt::Long;
use File::Find;

my ($help, $warning, $critical) = undef;

my $cpt=-1;
my $alertc = 0;
my $alertw = 0;
my $msginfo = "";

Getopt::Long::Configure ("bundling");
        GetOptions(
                'h:s'   => \$help,                   'help:s'           => \$help,
                'w:s'   => \$warning,                'warning:s'        => \$warning,
                'c:s'   => \$critical,               'critical:s'       => \$critical,
                );

    if (defined $help) {print "Exemple :\n
        my_script.pl -w 500 -c 1000\n
        or\n
        my_script.pl -warning 500 -critical 1000\n";cli(3,'Help Menu');}

    if (!defined $warning) {
        cli(3, 'Please, use -warning or -w');
    }
    if (!defined $critical) {
        cli(3, 'Please, use -critical or -c');
    }
    if ($warning >= $critical) {
        cli(3, 'Warning > = Critical');
    }

########### Main ###############

my @check = ('active','bounce','deferred','incoming','maildrop');

foreach my $folder (@check) {
    cal_mail("/var/spool/postfix/$folder");
    alert($folder);
}

mgtalert();

######### Functions #############

sub cal_mail {
    my ($directory) = @_;
    $cpt = -1;
	find(\&cpt, $directory);
}

sub cli {
        use Switch;
        my ($code, $message) = @_;

        switch ($code) {
                #It's OK
                case '0' {
                    print "$message";
                exit 0;
                }
                #It's Warning
                case '1'{
                    print "$message";
                    exit 1;
                }
                #It's Critical
                case '2'{
                    print "$message";
                    exit 2;
                }
                #Unkwnown
                case '3'{
                    print "$message";
                     exit 3;
                }

        }
}

sub cpt { 
    $cpt++;
}

sub alert {
my ($categorie) = @_;

if ($cpt >= $critical) {
    message("Critical : $categorie => $cpt | ");
    $alertc = 1;
    return;
 }
if ($cpt >= $warning) {
    message("Warning : $categorie => $cpt | ");
    $alertw = 1;
    return;
 }
 if ($cpt <= $warning) {
    message("OK : $categorie => $cpt | ");
 }
}

sub message {
    my ($message) = @_;
    $msginfo = $msginfo.$message;
}

sub mgtalert {
    if ($alertc == 1) {
        cli(2, $msginfo);
    }

    if ($alertw == 1) {
        cli(1, $msginfo);
     }

    cli(0, $msginfo);
}